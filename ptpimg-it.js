#!/usr/bin/env node
"use strict";

const version = "1.0.0";
const fs = require("fs");
const path = require("path");
const axios = require("axios");
const cheerio = require("cheerio");
const cookie = fs
  .readFileSync(path.join(__dirname, "red-cookie.txt"), "utf8")
  .replace(/\r|\n/g, "");
const ptp_api = fs
  .readFileSync(path.join(__dirname, "ptp-api.txt"), "utf8")
  .replace(/\r|\n/g, "");

if (process.argv.length !== 3) {
  console.log("Syntax: ptpimg-it 'https://f4.bcbits.com/img/a1421408453_10.jpg'\n");
  console.log("\tOR\n")
  console.log("Syntax: ptpimg-it 'https://redacted.ch/torrents.php?id=1111647&torrentid=2399614'");
  process.exit(1);
}

const id =
  process.argv[2].match(/^https:\/\/redacted.ch.*\?torrentid=([0-9]+)/) ||
  process.argv[2].match(/^https:\/\/redacted.ch.*\?id=([0-9]+)/) ||
  process.argv[2].match(/^https?:\/\/.+(?:\.jpg|\.png|\.jpeg)$/);

if (id === null) {
  console.log("ERROR: unable to capture group or torrent id.");
  process.exit(1);
}

const redAPI = async (id, getAuth = false) => {
  try {
    const action = getAuth
      ? "index"
      : id[0].match(/torrentid/)
      ? "torrent"
      : "torrentgroup";
    return axios.get(
      `https://redacted.ch/ajax.php?action=${action}&id=${id[1]}`,
      {
        headers: {
          Cookie: cookie,
          "User-Agent": `ptpimg-it ${version}`
        }
      }
    );
  } catch (error) {
    console.log(error);
  }
};

const encoded = params => {
  return Object.keys(params)
    .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
    .join("&");
};

const ptpUP = async cover => {
  console.log(`OLD: ${cover}`);
  return axios.post(
    "https://ptpimg.me/upload.php",
    encoded({ "link-upload": cover, api_key: ptp_api })
  );
};

if (id[1] === undefined) { // cover URL, POST only to PTP, not RED
  ptpUP(id[0])
    .then(({ data: [{ code, ext }] }) => {
      console.log(`NEW: https://ptpimg.me/${code}.${ext}`);
    })
    .catch(({ response }) =>
      console.error(`PTP: ${response.status}: ${response.statusText}`)
    );
} else {
  redAPI(id).then(async ({ data: { status, response } }) => {
    if (status !== "success") {
      console.log("RED API request failed; aborting.");
      console.log(`STATUS: ${status}`);
      process.exit(1);
    }
    const {
      group: {
        wikiImage: cover,
        wikiBody,
        id: groupid,
        releaseType: releasetype
      }
    } = response;
    if (cover.match(/ptpimg/)) {
      console.log(`ABORT: cover URL already points to ptpimg: ${cover}`);
      process.exit(1);
    }
    const { data: [{ code, ext }] } = await ptpUP(cover);
    const image = `https://ptpimg.me/${code}.${ext}`;
    console.log(`NEW: ${image}`);
    const { data } = await redAPI([null, null], "authKey");
    if (data.status !== "success") {
      console.log("RED API request failed; aborting.");
      process.exit(1);
    }
    const { response: { authkey } } = data;
    const params = {
      image,
      auth: authkey,
      releasetype,
      groupid,
      summary: "Move cover to PTPIMG",
      action: "takegroupedit"
    };
    const { data: html } = await axios.get(
      `https://redacted.ch/torrents.php?action=editgroup&groupid=${groupid}`,
      {
        responseType: "document",
        headers: { 
          Cookie: cookie,
          "User-Agent": `ptpimg-it ${version}`
        }
      }
    );
    const $ = cheerio.load(html);
    params["body"] = $("#textarea_wrap_0")
      .text()
      .replace(/(?:^\n\t)|(?:\n$)/g, "");
    axios
      .post("https://redacted.ch/torrents.php", encoded(params), {
        headers: { 
          Cookie: cookie,
          "User-Agent": `ptpimg-it ${version}`
        },
        responseType: 'document'
      })
      .then(({ status }) => {
        if (status == 200) {
          console.log("RED: Updated.");
        } else {
          console.log("RED: Failed!");
        }
      });
  })
  .catch(error => {
    console.log(error);
  });
}
